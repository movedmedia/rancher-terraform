#!/bin/bash

RANCHER_TOKEN="YOUR_RANCHER_TOKEN"
RANCHER_URL="https://YOUR_RANCHER_URL"
RANCHER_CLUSTER="CLUSTER_ID"
INDEX=1
for row in $(curl -s "${RANCHER_URL}/v3/clusters/${RANCHER_CLUSTER}/nodes?worker=true&state=active" -H "Authorization: bearer ${RANCHER_TOKEN}" | jq -r '.data[].ipAddress'); do
	echo "set server nodes/websrv${INDEX} addr ${row} port 443" | socat stdio /var/run/haproxy/admin.sock
	echo "set server nodes/websrv${INDEX} state ready" | socat stdio /var/run/haproxy/admin.sock
	let INDEX=${INDEX}+1
done

for ((i=INDEX;i<=100;i++))
do
   	echo "disable server nodes/websrv$i" | socat stdio /var/run/haproxy/admin.sock > /dev/null
done

echo "show servers state" | socat stdio /var/run/haproxy/admin.sock > /var/state/haproxy/global
