resource "digitalocean_droplet" "haproxy" {
    image = "ubuntu-16-04-x64"
    name = "haproxy"
    region = "nyc1"
    size = "512mb"
    private_networking = true
    ssh_keys = "${var.ssh_keys}"
  connection {
      user = "root"
      type = "ssh"
      private_key = "${file(var.pvt_key)}"
      timeout = "2m"
  }
  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install haproxy
      "sudo add-apt-repository -y ppa:vbernat/haproxy-${var.haproxy_version}",
      "sleep 10",
      "sudo apt-get update",
      "sudo apt-get -y install haproxy socat jq",

      # download haproxy conf
      "sudo wget https://bitbucket.org/movedmedia/rancher-terraform/raw/master/haproxy.cfg -O /etc/haproxy/haproxy.cfg",
      "sudo wget https://bitbucket.org/movedmedia/rancher-terraform/raw/master/syncnodes.sh -O /root/syncnodes.sh && sudo chmod +x /root/syncnodes.sh",

      "sudo mkdir /var/state/haproxy -p",
 
      # restart haproxy to load changes
      "sudo service haproxy restart"
    ]
  }
}
