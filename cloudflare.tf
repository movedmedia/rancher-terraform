provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

variable "cloudflare_email" {
  default = ""
}

variable "cloudflare_token" {
  default = ""
}

variable "cloudflare_zone" {
  default = ""
}

resource "cloudflare_record" "rancher" {
  domain  = "${var.cloudflare_zone}"
  name    = "rancher"
  value   = "${digitalocean_droplet.rancherserver.ipv4_address}"
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "cluster" {
  domain  = "${var.cloudflare_zone}"
  name    = "cluster"
  value   = "${digitalocean_droplet.haproxy.ipv4_address}"
  type    = "A"
  ttl     = 1
  proxied = true
}

